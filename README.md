# The Mysterious Stranger Game 
I made this game in a week for my English practice (not time spent coding; time 
from start of the project to the end). Because I had never developed a game 
before, I decided to keep things simple and not worry about object-oriented
principles--I wasn't sure what needed access to what in terms of the game's 
"component" structure

# Libraries/Dependencies
Phaser

# Demo
https://navbryce.gitlab.io/english-project/