
// titleScreen
var titleGroup;
var titleScreen = false;
var titleFont = 32;
var titleStyle = { font: "bold " + titleFont + "px blocky", fill: "#fff" };
var titleText = []; // an array of text objects


// text world notifications
var textNotArray = [];
var line = [];

var wordIndex = 0;
var lineIndex = 0;

var wordDelay = 120;
var lineDelay = 400;
var deleteDelay = 4000;
var numberOfLines = 0;
const maxNumberOfLines = 5;

// music/sound notifications
var currentMusic = null;

/**
sends notification

@param notification - notification object
**/
function addNotification(notification) {
  if (notification.gameComplete != null) {
    gameComplete = true;
    scoreElement.visible = false;
    notification.text = ["The End.", "Your Score", "" + score];
    notification.title = true;
  }
  if (notification.text != null || notification.convo != null) {
    if (notification.title) {
      addTitleText(notification.text, notification.end);
    } else {
      var isConvo = notification.convo != null;

      // the id effectively does not thing in this line
      var conversation;
      if (isConvo) {
        // load in the appropriate conversation
        convo.load(dialogConfig[notification.convo]);
        conversation = convo.get(1, 'dialog');
      } else {
        conversation = notification.text;
      }
      var result = setCurrentConversation(conversation, notification.convo,
        notification.end);

      /* if the conversation did not launch
      (because of an existing conversation),  then  make sure the count
      down is at 0 */
      result || (notification.lastNotified = null);
    }
  }

  notification.audio == null || addAudioNotification(notification.audio);
  notification.level == null || switchLevel(notification.level);
  notification.show == null || showSprites(notification.show);
  notification.hide == null || hideSprites(notification.hide);
  notification.score == null || changeScore(notification.score);
  notification.movePlayer == null || playerMoveNotify(notification.movePlayer)
}

function addAudioNotification (audioObject) {
  playSound(audioObject);
}

/**
adds to the list of notifications

@param lines - expects an array of strings
**/
function addTextNotification (text) {
  text.forEach((line) => {
    textNotArray.push(line);
  });
}

function addTitleText (text, endNotification) {
  titleGroup.visible = true;
  titleScreen = true;
  var copy = text.map((line) => {
    return line;
  });
  addTitleLine(copy, endNotification);
}

function addTitleLine (textArray, endNotification) {
  // get text
  var line = textArray.splice(0, 1);

  // determine position and creating text element
  var y = gameHeight / 2 + ((titleFont + 30) * (titleText.length - 1));
  var textElement = game.add.bitmapText(gameWidth / 2, y, 'blocky', line, titleFont);
  textElement.anchor.setTo(0.5);
  titleGroup.add(textElement);

  // append text element to the array of text
  titleText.push(textElement);

  // recursive calls for other lines
  if (textArray.length > 0) {
    game.time.events.add(1000, () => {
      addTitleLine(textArray, endNotification);
    }, this);
  } else {
    // call the end notification if there is one
    !!endNotification && notify(endNotification);
    gameComplete || game.time.events.add(2000, endTitleScreen, this);
  }
}

function deleteLine () {
  if (numberOfLines >= 0 && !titleScreen) {
    var firstLineEnd = text.text.search("\n");
    if (firstLineEnd == -1) {
      firstLineEnd = text.text.length - 2;
    }
    text.text = text.text.substring(firstLineEnd + 2, text.text.length);
    numberOfLines--;
  }
}

function endTitleScreen () {
  titleGroup.visible = false;
  titleText = titleText.filter((element) => {
    titleGroup.remove(element);
    return false;
  })
  titleScreen = false;
}

function drawTitleSprite () {
  titleGroup = game.add.group();
  var rectangle;
  var bmd = game.add.bitmapData(gameWidth, gameHeight);
  bmd.ctx.beginPath();
  bmd.ctx.rect(0, 0, gameWidth, gameHeight);
  bmd.ctx.fillStyle = 'black';
  bmd.ctx.fill();
  rectangle = game.add.sprite(0, 0, bmd);
  titleGroup.add(rectangle);
  titleGroup.fixedToCamera = true;
  titleGroup.visible = false;
}

function initialize () {
  dialogLoad();
  drawTitleSprite();
  nextLine();
}

function playSound (audioObject) {
  if (!disableAudio) {
    var phaserAudio = audioObjects[audioObject.sound];
    if (audioObject.isMusic) {
      if (currentMusic != null) {
        currentMusic.stop();
      }
      currentMusic = phaserAudio;
    }

    phaserAudio.play();
  }
}

function nextLine() {
  if (textNotArray.length == 0)
  {
      setTimeout(nextLine, 100);
  } else {

    //  Split the current line on spaces, so one word per array element
    line = textNotArray.splice(0, 1)[0].split(' ');
    numberOfLines++;

    // delete excess lines
    if (numberOfLines >= maxNumberOfLines) {
      deleteLine();
    }

    //  Reset the word index to zero (the first word in the line)
    wordIndex = 0;

    //  Call the 'nextWord' function once for each word in the line (line.length)
    game.time.events.repeat(wordDelay, line.length, nextWord, this);

    //  Advance to the next line
    lineIndex++;
  }
}



function nextWord () {

  //  Add the next word onto the text string, followed by a space
  text.text = text.text.concat(line[wordIndex] + " ");

  //  Advance the word index to the next word in the line
  wordIndex++;

  //  Last word?
  if (wordIndex === line.length)
  {
      //  Add a carriage return
      text.text = text.text.concat("\n");

      //  Get the next line after the lineDelay amount of ms has elapsed
      game.time.events.add(lineDelay, nextLine, this);

      // an entire line has been added so delete it
      game.time.events.add(deleteDelay, deleteLine, this);

  }
}
function textPreload () {
  game.load.bitmapFont('blocky', 'assets/fonts/bitmapFonts/carrier_command.png', 'assets/fonts/bitmapFonts/carrier_command.xml');
}
