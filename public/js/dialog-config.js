var dialogConfig = {
  "first": {
    actor: [
      {
        id: 1,
        name: {
          first: 'Irving',
          last: '',
        },
      },
      {
        type: 'actor',
        id: 2,
        name: {
          first: 'Sophie',
          last: '',
        },
      }
    ],
    answer: [
      {
        id: 1,
        emotion: 1,
        text: 'Because I inherited it from my dead brother.'
      },
      {
        id: 2,
        emotion: 7,
        text: "Let's save this conversation for later. It's cold."
      }
    ],
    dialog: [
      {
        id: 1,
        type: 'question',
        actorId: 2,
        emotion: 0,
        text: '{actor:0x001}, why in the hell did we decide to move to this country?',
        answers: [
          {
            answerId: 1,
            next: 2
          },
          {
            answerId: 2,
            next: 3
          }
        ],
      },
      {
        id: 2,
        type: 'statement',
        actorId: 2,
        emotion: 1,
        text: "I'm sorrying for bringing it up. Let's just get to the house. We should be pretty close now. I'm going to wait in the car. Call me when you get there [+100].",
        score: 100,
        next: 4
      },
      {
        id: 3,
        type: 'statement',
        actorId: 2,
        emotion: 1,
        text: "Stop acting like a child! Let's just get to the house. We should be pretty close now. I'm going to wait in the car. Call me when you get there [-500].",
        score: -500,
        next: 4
      },
      {
        id: 4,
        type: 'statement',
        actorId: 1,
        emotion: 4,
        text: "Sure. I'll just walk through these creepy woods by myself.",
        next: false
      }
    ],
    emotion: [
      'Happy',
      'Curious',
      'Joking',
      'Joyful',
      'Playful',
      'Sad',
      'Sorrowful',
      'Tired'
    ]
  },
  'wolves': {
    actor: [
      {
        id: 1,
        name: {
          first: 'Irving',
          last: '',
        },
      },
      {
        type: 'actor',
        id: 2,
        name: {
          first: 'Sophie',
          last: '',
        },
      },
      {
        type: 'actor',
        id: 3,
        name: {
          first: 'Wolves',
          last: '',
        }
      },
      {
        type: 'actor',
        id: 4,
        name: {
          first: 'Action prompt',
          last: ''
        }
      }
    ],
    answer: [
      {
        id: 1,
        emotion: 7,
        text: "Shit. *Throw shoe at them*",
      },
      {
        id: 2,
        emotion: 7,
        text: "Bark back at them. Assert your dominance",
      },
      {
        id: 3,
        emotion: 7,
        text: "Flee by running past them",
      },
      {
        id: 4,
        emotion: 7,
        text: "Run and flee past them",
      },
      {
        id: 5,
        emotion: 7,
        text: "It's pretty clear. You haver to run to the right.",
      }
    ],
    dialog: [
      {
        id: 1,
        type: 'question',
        actorId: 1,
        emotion: 3,
        text: 'Wooof. Bark. Woof *Gnashing of teeth*',
        answers: [
          {
            answerId: 1,
            next: 2
          },
          {
            answerId: 2,
            next: 2
          }
        ]
      },
      {
        id: 2,
        type: 'question',
        actorId: 4,
        emotion: 3,
        text: "That didn't work. What should you do?",
        answers: [
          {
            answerId: 3,
            next: 3
          },
          {
            answerId: 4,
            next: 3
          },
          {
            answerId: 5,
            next: 3
          }
        ]
      },
      {
        id: 3,
        type: 'statement',
        actorId: 3,
        emotion: 3,
        text: 'They look mad! Run!',
        next: false
      }
    ],
    emotion: [
      'Happy',
      'Curious',
      'Joking',
      'Joyful',
      'Playful',
      'Sad',
      'Sorrowful',
      'Tired'
    ]
  },
  'azzo-first': {
    actor: [
      {
        id: 1,
        name: {
          first: 'Irving',
          last: '',
        },
      },
      {
        id: 2,
        name: {
          first: 'Observation',
          last: '',
        }
      }
    ],
    answer: [
    ],
    dialog: [
      {
        id: 1,
        type: 'statement',
        actorId: 2,
        emotion: 1,
        text: 'You notice the stranger is wearing rusted armor and looks very pale',
        next: 2
      },
      {
        id: 2,
        type: 'statement',
        actorId: 1,
        emotion: 1,
        text: "Thanks for scaring those wolves away. This might seem like an odd question. But why are you wearing armor?",
        next: 3
      },
      {
        id: 3,
        type: 'statement',
        actorId: 2,
        emotion: 1,
        text: 'The mysterious man walks back into the woods. How strange.',
        next: false
      }
    ],
    emotion: [
      'Happy',
      'Curious',
      'Joking',
      'Joyful',
      'Playful',
      'Sad',
      'Sorrowful',
      'Tired'
    ]
  },
  'mailbox': {
    actor: [
      {
        id: 1,
        name: {
          first: 'Irving',
          last: '',
        },
      },
      {
        id: 2,
        name: {
          first: 'Action Prompt',
          last: '',
        }
      }
    ],
    answer: [
      {
        id: 1,
        emotion: 7,
        text: "Of course! I love reading the mail."
      },
      {
        id: 2,
        emotion: 7,
        text: "I'll do it later.",
      }
    ],
    dialog: [
      {
        id: 1,
        type: 'question',
        actorId: 2,
        emotion: 1,
        text: 'Open the mail box?',
        answers: [
          {
            answerId: 1,
            next: 2
          },
          {
            answerId: 2,
            next: 3
          }
        ]
      },
      {
        id: 2,
        type: 'statement',
        actorId: 1,
        emotion: 1,
        text: "There's a dead bird in there; it looks really withered and dry. Yuck! [-100]",
        score: -100,
        next: false
      },
      {
        id: 3,
        type: 'statement',
        actorId: 1,
        emotion: 1,
        text: "Smart. It's probably filled with taxes and spam [+10]",
        score: 10,
        next: false
      }
    ],
    emotion: [
      'Happy',
      'Curious',
      'Joking',
      'Joyful',
      'Playful',
      'Sad',
      'Sorrowful',
      'Tired'
    ]
  },
  'wife-return': {
    actor: [
      {
        id: 1,
        name: {
          first: 'Irving',
          last: '',
        },
      },
      {
        id: 2,
        name: {
          first: 'Sophie',
          last: '',
        }
      }
    ],
    answer: [
      {
        id: 1,
        emotion: 7,
        text: "You sure that's a good idea?"
      },
      {
        id: 2,
        emotion: 7,
        text: "I love dinner."
      }
    ],
    dialog: [
      {
        id: 1,
        type: 'statement',
        actorId: 1,
        emotion: 1,
        text: 'I saw an odd-looking man while walking up here. He was wearing armor and looked...dangerous. At least the house is nice.',
        next: 2
      },
      {
        id: 2,
        type: 'question',
        actorId: 2,
        emotion: 1,
        text: "I know, Irving. I saw him too. I actually invited him to dinner. It's good to get know the know the neighbors.",
        answers: [
          {
            answerId: 1,
            next: 3
          },
          {
            answerId: 2,
            next: 4
          }
        ]
      },
      {
        id: 3,
        type: 'statement',
        actorId: 2,
        emotion: 1,
        text: "Hmmmmm. You might be right. We'll be extra careful when he's here [+200]",
        score: 200,
        next: 6
      },
      {
        id: 4,
        type: 'statement',
        actorId: 2,
        emotion: 1,
        text: "*Says sarcastically* Thanks for that tid bit. Very inciteful [+150]",
        score: 150,
        next: 5
      },
      {
        id: 5,
        type: 'statement',
        actorId: 1,
        emotion: 1,
        text: "Any time.",
        next: 6
      },
      {
        id: 6,
        type: 'statement',
        actorId: 1,
        emotion: 1,
        text: "",
        next: false,
        autoClose: true
      }
    ],
    emotion: [
      'Happy',
      'Curious',
      'Joking',
      'Joyful',
      'Playful',
      'Sad',
      'Sorrowful',
      'Tired'
    ]
  },
  'dinner': {
    actor: [
      {
        id: 1,
        name: {
          first: 'Irving',
          last: '',
        },
      },
      {
        id: 2,
        name: {
          first: 'Sophie',
          last: '',
        }
      },
      {
        id: 3,
        name: {
          first: 'Azzo',
          last: '',
        }
      },
      {
        id: 4,
        name: {
          first: 'Observation',
          last: '',
        }
      }
    ],
    answer: [
      {
        id: 1,
        emotion: 7,
        text: "Of course, come in."
      },
      {
        id: 2,
        emotion: 7,
        text: "Hell no. You look practically dead."
      },
      {
        id: 3,
        emotion: 7,
        text: "You don't eat? How are you alive..."
      },
      {
        id: 4,
        emotion: 7,
        text: "<Say nothing>."
      }
    ],
    dialog: [
      {
        id: 1,
        type: 'question',
        actorId: 3,
        emotion: 1,
        text: 'You said I was invited? *Looking with conptempt at Irving*',
        answers: [
          {
            answerId: 1,
            next: 3
          },
          {
            answerId: 2,
            next: 2
          }
        ]
      },
      {
        id: 2,
        type: 'statement',
        actorId: 2,
        emotion: 1,
        text: "Irving! Why are you being so cruel to our neighbor? Of course you can come in [+50].",
        score: 50,
        next: 3
      },
      {
        id: 3,
        type: 'statement',
        actorId: 2,
        emotion: 1,
        text: "So, what's your name? Would you like some dinner?",
        next: 4
      },
      {
        id: 4,
        type: 'question',
        actorId: 3,
        emotion: 1,
        text: "My name is Azzo of Klatka. I don't eat, only drink. ",
        answers: [
          {
            answerId: 3,
            next: 5
          },
          {
            answerId: 4,
            next: 6
          }
        ]
      },
      {
        id: 5,
        type: 'statement',
        actorId: 4,
        emotion: 1,
        text: 'Azzo berates Irving for the next 20 minutes about how he "lives" life and Irving does not [-200].',
        score: -200,
        next: 6
      },
      {
        id: 6,
        type: 'statement',
        actorId: 4,
        emotion: 1,
        text: "Azzo continues to talk with contempt to Irving and respect to Sophie for 3 hours more hours. Then, he disappears into the night.",
        next: 7
      },
      {
        id: 7,
        type: 'statement',
        actorId: 4,
        emotion: 1,
        autoClose: true,
        next: false
      }
    ],
    emotion: [
      'Happy',
      'Curious',
      'Joking',
      'Joyful',
      'Playful',
      'Sad',
      'Sorrowful',
      'Tired'
    ]
  },
  'morning-after': {
    actor: [
      {
        id: 1,
        name: {
          first: 'Irving',
          last: '',
        },
      },
      {
        id: 2,
        name: {
          first: 'Sophie',
          last: '',
        }
      }
    ],
    answer: [
      {
        id: 1,
        emotion: 7,
        text: "My brother died and now you're doing this..."
      },
      {
        id: 2,
        emotion: 7,
        text: "<Remain silent>"
      }
    ],
    dialog: [
      {
        id: 1,
        type: 'statement',
        actorId: 2,
        emotion: 1,
        text: "*Sophie walks downstairs.* I don't feel so well...And I had the weirdest dream about Azzo biting me.",
        next: 2
      },
      {
        id: 2,
        type: 'statement',
        actorId: 1,
        emotion: 1,
        text: "You're dreaming about Azzo now!?!? That guy is such an ass and a creep.",
        score: 50,
        next: 3
      },
      {
        id: 3,
        type: 'question',
        actorId: 2,
        emotion: 1,
        text: "I don't know what to tell you. Azzo understands my free personality.",
        answers: [
          {
            answerId: 1,
            next: 4
          },
          {
            answerId: 2,
            next: 5
          }
        ]
      },
      {
        id: 4,
        type: 'statement',
        actorId: 2,
        emotion: 1,
        text: "You're right. I think this illness is getting to me [+400].",
        score: 400,
        next: 6
      },
      {
        id: 5,
        type: 'statement',
        actorId: 2,
        emotion: 1,
        text: "Don't be like that [-200].",
        score: -200,
        next: 6
      },
      {
        id: 6,
        type: 'statement',
        actorId: 2,
        emotion: 1,
        text: "",
        autoClose: true
      }
    ],
    emotion: [
      'Happy',
      'Curious',
      'Joking',
      'Joyful',
      'Playful',
      'Sad',
      'Sorrowful',
      'Tired'
    ]
  },
  'woislaw': {
    actor: [
      {
        id: 1,
        name: {
          first: 'Irving',
          last: '',
        },
      },
      {
        id: 2,
        name: {
          first: 'Woislaw',
          last: '',
        }
      },
      {
        id: 3,
        name: {
          first: 'Observation',
          last: '',
        }
      }
    ],
    answer: [
      {
        id: 1,
        emotion: 7,
        text: "She's actually really ill. She's always pale and..."
      },
      {
        id: 2,
        emotion: 7,
        text: "We have a very mysterious neighbor..."
      }
    ],
    dialog: [
      {
        id: 1,
        type: 'statement',
        actorId: 2,
        emotion: 1,
        text: "Hi Irving! I'm so excited to see this new house of yours... Weird area.",
        next: 2
      },
      {
        id: 2,
        type: 'statement',
        actorId: 1,
        emotion: 1,
        text: "I know. V-e-r-y weird area. *Abruptly* Are you going on another tour of duty soon?",
        score: 50,
        next: 3
      },
      {
        id: 3,
        type: 'question',
        actorId: 2,
        emotion: 1,
        text: "Yup. But let's talk about more positive things. How's Sophie?",
        answers: [
          {
            answerId: 1,
            next: 4
          },
          {
            answerId: 2,
            next: 4
          }
        ]
      },
      {
        id: 4,
        type: 'statement',
        actorId: 3,
        emotion: 1,
        text: "You go on for the next hour discussing the strange series of events that have transpired in the past month. Throughout the conversation, Woislaw's expression darkens.",
        next: 5
      },
      {
        id: 5,
        type: 'statement',
        actorId: 2,
        emotion: 1,
        text: "I think I know what's happening here. You and Sophie must do exactly what I say. Meet me at the oak tree by the Castle tonight.",
        next: false
      }
    ],
    emotion: [
      'Happy',
      'Curious',
      'Joking',
      'Joyful',
      'Playful',
      'Sad',
      'Sorrowful',
      'Tired'
    ]
  },
  'woislawTree': {
    actor: [
      {
        id: 1,
        name: {
          first: 'Irving',
          last: '',
        },
      },
      {
        id: 2,
        name: {
          first: 'Woislaw',
          last: '',
        }
      },
      {
        id: 3,
        name: {
          first: 'Observation',
          last: '',
        }
      }
    ],
    answer: [
      {
        id: 1,
        emotion: 7,
        text: "She's actually really ill. She's always pale and..."
      },
      {
        id: 2,
        emotion: 7,
        text: "We have a very mysterious neighbor..."
      }
    ],
    dialog: [
      {
        id: 1,
        type: 'statement',
        actorId: 2,
        emotion: 1,
        text: "Irving. Listen CLOSELY. If you deviate from my instructions, you will kill Sophie",
        next: 2
      },
      {
        id: 2,
        type: 'statement',
        actorId: 2,
        emotion: 1,
        text: "You must go to the catacombs. The main entrance in the chapel is blocked. You must descend a narrow set of stairs. You will know it when you see it.",
        next: 3
      },
      {
        id: 3,
        type: 'statement',
        actorId: 2,
        emotion: 1,
        text: "Find the {{VARIABLE}} coffin. Hammer three nails into it while I give the Credo",
        next: 4
      },
      {
        id: 4,
        type: 'statement',
        actorId: 2,
        emotion: 1,
        text: "Begin.",
        next: false
      }
    ],
    emotion: [
      'Happy',
      'Curious',
      'Joking',
      'Joyful',
      'Playful',
      'Sad',
      'Sorrowful',
      'Tired'
    ]
  },
  'secret': {
    actor: [
      {
        id: 1,
        name: {
          first: 'Action Prompt',
          last: '',
        }
      }
    ],
    answer: [
      {
        id: 1,
        emotion: 7,
        text: "You descend the stairs."
      }
    ],
    dialog: [
      {
        id: 1,
        type: 'question',
        actorId: 1,
        emotion: 1,
        text: "You see a narrow set of stairs through the door. It smells damp and old. You hear Woislaw chant the Credo behind you.",
        answers: [
          {
            answerId: 1,
            next: 2
          }
        ]
      },
      {
        id: 2,
        type: 'statement',
        actorId: 1,
        emotion: 1,
        text: "",
        next: false,
        autoClose: true
      }
    ],
    emotion: [
      'Happy',
      'Curious',
      'Joking',
      'Joyful',
      'Playful',
      'Sad',
      'Sorrowful',
      'Tired'
    ]
  },
  'correct-coffin': {
    actor: [
      {
        id: 1,
        name: {
          first: 'Action Prompt',
          last: '',
        }
      },
      {
        id: 2,
        name: {
          first: 'Observation',
          last: '',
        }
      }
    ],
    answer: [
      {
        id: 1,
        emotion: 7,
        text: "Yes. Nail the coffin."
      },
      {
        id: 2,
        emotion: 7,
        text: "No. Move on."
      }
    ],
    dialog: [
      {
        id: 1,
        type: 'question',
        actorId: 1,
        emotion: 1,
        text: "You see a coffin. Are you sure it's the right coffin? Time is precious.",
        answers: [
          {
            answerId: 1,
            next: 2
          },
          {
            answerId: 2,
            next: 3
          }
        ]
      },
      {
        id: 2,
        type: 'statement',
        actorId: 1,
        emotion: 1,
        text: "You are correct in your choice. Blood starts to seep from the coffin [+2500]",
        score: 2500,
        next: 4
      },
      {
        id: 3,
        type: 'statement',
        actorId: 1,
        emotion: 1,
        text: "You fool. This was the right coffin. Time has been wasted. You nail it and blood starts to seep from it. [-1000]",
        score: -1000,
        next: 4
      },
      {
        id: 4,
        type: 'statement',
        actorId: 2,
        emotion: 1,
        text: "You are overwhelmed by a tiredness.....",
        score: 0,
        next: 5
      },
      {
        id: 5,
        type: 'statement',
        actorId: 1,
        emotion: 1,
        text: "",
        autoClose: true,
        next: false
      }
    ],
    emotion: [
      'Happy',
      'Curious',
      'Joking',
      'Joyful',
      'Playful',
      'Sad',
      'Sorrowful',
      'Tired'
    ]
  },
  'incorrect-coffin': {
    actor: [
      {
        id: 1,
        name: {
          first: 'Action Prompt',
          last: '',
        }
      }
    ],
    answer: [
      {
        id: 1,
        emotion: 7,
        text: "Yes. Nail the coffin."
      },
      {
        id: 2,
        emotion: 7,
        text: "No. Move on."
      }
    ],
    dialog: [
      {
        id: 1,
        type: 'question',
        actorId: 1,
        emotion: 1,
        text: "You see a coffin. Are you sure it's the right coffin? Time is precious.",
        answers: [
          {
            answerId: 1,
            next: 3
          },
          {
            answerId: 2,
            next: 2
          }
        ]
      },
      {
        id: 2,
        type: 'statement',
        actorId: 1,
        emotion: 1,
        text: "Move on then.",
        score: 0,
        next: false
      },
      {
        id: 3,
        type: 'statement',
        actorId: 1,
        emotion: 1,
        text: "You fool. This was the wrong coffin. Time has been wasted [-1000].",
        score: -1000,
        next: false
      }
    ],
    emotion: [
      'Happy',
      'Curious',
      'Joking',
      'Joyful',
      'Playful',
      'Sad',
      'Sorrowful',
      'Tired'
    ]
  }
};
