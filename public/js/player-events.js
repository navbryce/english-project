var timeSinceLastDamage = null;
function playerDeath () {
  if (timeSinceLastDamage == null) {
    /*
    starts the "check loop"
    only enter the loop once AKA the first time the dmg function is called
    when timeSinceLastDamage is null (we don't want to start the loop
    a thousand times)
    */
    timeSinceLastDamage = new Date().getTime();
    checkIfDamageEnded();
  } else {
    timeSinceLastDamage = new Date().getTime();
  }

  tintPlayerForDamage();

  // update the player's score
  changeScore(-250);
}

var dmgCounter = 0;
var tick = 9; // (when taking damage) every number of tick, the player will change color
const defaultColor = 0xffffff;
const hurtColor = 0xff0000;
var currentColor = defaultColor;

function tintPlayerForDamage  () {
  dmgCounter = dmgCounter % tick;
  if (dmgCounter === 0) {
    currentColor = currentColor === defaultColor ? hurtColor : defaultColor;
    player.tint = currentColor;
  }
  dmgCounter++;
}

function checkIfDamageEnded () {
  // checks to see if the damage event is over
  var delta = new Date().getTime() - timeSinceLastDamage;
  if (delta > 250) {
    player.tint = defaultColor; // reset the sprite's color
    timeSinceLastDamage = null;
    dmgCounter = 0; // reset the dmg counter
  } else {
    game.time.events.add(250, checkIfDamageEnded, this); // check again if 500 ms
  }
}

function movePlayer (newTileX, newTileY) {
  var x = convertToPx(newTileX);
  var y = convertToPx(newTileY);
  player.x = x;
  player.y = y;
}

function playerMoveNotify (notification) {
  movePlayer(notification.xTile, notification.yTile);
}
