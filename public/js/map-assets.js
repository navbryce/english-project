// note: sprites are not resized
var spriteMap = {}; // a map for all the sprites where key is the element's id
// sprite library
var mapAssets = [
  {
    name: 'car',
    path: 'car_small.png'
  },
  {
    name: 'wife_left',
    path: 'wife_left.png'
  },
  {
    name: 'skeleton',
    path: 'skeleton.png'
  },
  {
    name: 'wolf-sprite',
    path: 'wolf.png'
  },
  {
    name: 'azzo-0',
    path: 'azzo_0.png'
  },
  {
    name: 'woislaw',
    path: 'woislaw.png'
  },
  {
    name: 'coffin',
    path: 'coffin.png'
  }
];

// actually places the sprites
var mapPlacement = [
  {
    name: 'car',
    xTile: '2',
    yTile: '19.5',
    collision: true
  },
  {
    name: 'wife_left',
    xTile: '25',
    yTile: '19.5',
    collision: false,
    id: 'initial-wife'
  },
  {
    name: 'skeleton',
    xTile: '180',
    yTile: '19.5',
    collision: false
  },
  {
    name: 'skeleton',
    xTile: '175',
    yTile: '24',
    collision: false
  },
  {
    name: 'skeleton',
    xTile: '185',
    yTile: '24',
    collision: false
  },
  {
    name: 'wolf-sprite',
    xTile: '233',
    yTile: '19.5',
    collision: false,
    id: 'wolf1',
    hide: true
  },
  {
    name: 'wolf-sprite',
    xTile: '234',
    yTile: '19.5',
    collision: false,
    id: 'wolf2',
    hide: true
  },
  {
    name: 'azzo-0',
    xTile: '267',
    yTile: '19.7',
    collision: false,
    id: 'azzo-wolf',
    hide: true
  },
  {
    name: 'azzo-0',
    xTile: '267',
    yTile: '19.7',
    collision: false,
    id: 'azzo-wolf',
    hide: true
  },
  {
    name: 'wife_left',
    xTile: '672',
    yTile: '19.7',
    collision: false,
    id: 'wife-door',
    hide: true,
    flipX: true
  },
  {
    name: 'azzo-0',
    id: 'azzo-dinner',
    xTile: '672',
    yTile: '19.7',
    collision: false,
    hide: true,
    flipX: true
  },
  {
    name: 'wife_left',
    xTile: '691',
    yTile: '19.7',
    collision: false,
    id: 'wife-dinner',
    hide: true
  },
  {
    name: 'wife_left',
    xTile: '715',
    yTile: '19.7',
    collision: false,
    id: 'wife-stairs',
    hide: true
  },
  {
    name: 'woislaw',
    xTile: '627',
    yTile: '19.7',
    collision: false,
    id: 'woislaw-path',
    hide: true,
    flipX: true
  },
  {
    name: 'woislaw',
    xTile: '487',
    yTile: '19.7',
    collision: false,
    id: 'woislaw-tree',
    hide: true,
    flipX: true
  },
  {
    name: 'coffin',
    xTile: '360',
    yTile: '69',
    colission: false,
    id: 'coffin-1'
  },
  {
    name: 'coffin',
    xTile: '334',
    yTile: '69',
    colission: false,
    id: 'coffin-2'
  },
  {
    name: 'coffin',
    xTile: '307',
    yTile: '69',
    colission: false,
    id: 'coffin-3'
  }
];

var mapAssetsGroup;

function mapAssetsPreload () {
  var basePath = rootPath + '/tilemap/map-objects/';
  mapAssets.forEach((asset) => {
    game.load.image(asset.name, basePath + asset.path);
  });
}

function mapAssetsLoad () {
  mapAssetsGroup = game.add.group();
  mapPlacement.forEach((placement) => {
    var image = game.cache.getImage(placement.name);
    var sprite = game.add.sprite(image.width, image.height, placement.name, 1);

    sprite.x = convertToPx(placement.xTile); // convert tile to px
    sprite.y = convertToPx(placement.yTile); // convert tile to px

    if (!!placement.flipX) {
      sprite.scale.x *= -1;
    }

    if (placement.collision) {
      game.physics.enable(sprite);
      sprite.body.allowGravity = false;
      sprite.body.immovable = true;
    }

    if (!!placement.id) { // if it has an id, store it in the map
      spriteMap[placement.id] = sprite;
    }

    if (!!placement.hide && placement.hide) {
      // hide if specified
      sprite.visible = false;
    }

    // add to group
    mapAssetsGroup.add(sprite);
  });
}

function hideSprite(id) {
  toggleSprite(id, false);
}

function hideSprites(ids) {
  ids.forEach((id) => {
    hideSprite(id);
  });
}

function showSprite(id) {
  toggleSprite(id, true);
}

function showSprites(ids) {
  ids.forEach((id) => {
    showSprite(id);
  });
}

function toggleSprite(id, newState) {
  if (!!spriteMap[id]) {
    spriteMap[id].visible = newState;
  }
}
