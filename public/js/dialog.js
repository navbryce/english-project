var diag;
var diagElements = [];
var diagFont = 14;
var diagFill = "#fff";
var diagFillHover = "rgb(90, 90, 90)";
var diagLineLength;
var diagStyle = { font: "bold " + diagFont * 1.5 + "px blocky", fill: diagFill };
var strokeWidth = 5;

// set in the preload
var diagHeight;
var diagWidth;
var diagX;
var diagY;

function dialogPreload () {
  diagHeight = gameHeight/3
  diagWidth = gameWidth/1.5;
  diagX = (gameWidth - diagWidth)/2;
  diagY = gameHeight - diagHeight + strokeWidth;
  diagLineLength = diagWidth / (1.5 * diagFont);
}

function dialogLoad () {
  diag = game.add.group();
  var rectangle;
  var bmd = game.add.bitmapData(gameWidth, gameHeight);
  var canDat = bmd.ctx;
  canDat.beginPath();

  canDat.rect(diagX, diagY, diagWidth, diagHeight);
  canDat.fillStyle = 'black';
  canDat.fill();
  canDat.lineWidth = strokeWidth;
  canDat.strokeStyle = 'red';
  canDat.stroke();
  rectangle = game.add.sprite(0, 0, bmd);
  diag.add(rectangle);
  diag.fixedToCamera = true;
  diagToggle(false);
}

function diagClear (hideDiag) {
  diagElements.forEach((element) => {
    diag.remove(element);
  });
  hideDiag && diagToggle(true);
}

/**
* if clickableLines is null, the callback will be called when the exit button
* is pressed.
*/
function diagSetText (text, clickableLines, callback) {
  var textLines = getLines(text);
  // clear the diag
  diagClear(false);

  // bind the callback
  callback = callback.bind(this);

  // exit button if no clickable lines
  clickableLines = clickableLines == null || clickableLines.length === 0 ?
    ['exit'] : clickableLines;

  var lineCounter = 0;
  var paddingY = 25;
  var lineTypes = [textLines, clickableLines];
  for (var typeCounter = 0; typeCounter < 2; typeCounter++) {
    let lines = lineTypes[typeCounter];
    if (lines != null) {
      var clickable = typeCounter === 1;
      var paddingX = clickable ? 200 : 50;

      lines.forEach((line) => {
        var x = diagX + paddingX;
        var y = diagY + (5 + diagFont) * lineCounter + paddingY;

        /* use normal text element for clickable response so the color
        can easily change */
        var textElement = clickable ? game.add.text(x, y, line, diagStyle) :
          game.add.bitmapText(x, y, 'blocky', line, diagFont);

        // click and hover handler
        if (clickable) {
          var answerIndex = lineCounter - textLines.length;
          textElement.text = answerIndex + 1 + ') ' + textElement.text;
          textElement.inputEnabled = true;
          // click handler
          textElement.events.onInputDown.add(() => {
            callback(answerIndex, line);
          }, this);
          // hover handlers
          textElement.events.onInputOver.add(() => {
            textElement.fill = diagFillHover;
          }, this);

          textElement.events.onInputOut.add(() => {
            textElement.fill = diagFill;
          }, this);

        }
        diag.add(textElement);
        diagElements.push(textElement);
        lineCounter++;
      });
    }
  }
  // make the dialog visible
  diagToggle(true);
}

function getLines (text) {
  var lines = [];
  while (text.length > diagLineLength) {
    var maxLine = text.substr(0, diagLineLength);

    // try to find the last space in the sentence
    var spaceIndex = -1;
    for (var indexCounter = maxLine.length - 1; indexCounter >= 0 &&
      spaceIndex === -1; indexCounter--) {
      var character = maxLine.substring(indexCounter, indexCounter + 1);
      if (character === ' ') {
        spaceIndex = indexCounter;
      }
    }
    // cut to the last space or cut the word if no spaces
    var cutIndex = spaceIndex !== -1 ? spaceIndex + 1 : diagLineLength;
    lines.push(text.substring(0, cutIndex));
    text = text.substring(cutIndex, text.length);
  }
  lines.push(text); // push the remaining text
  return lines;
}

function diagToggle (newState) {
  diag.visible = newState;
}
