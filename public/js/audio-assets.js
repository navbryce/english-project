var audioAssets = [
  {
    name: "forest",
    path: "forest.mp3"
  },
  {
    name: "wolf",
    path: "wolf.mp3"
  },
  {
    name: "end",
    path: "end.mp3"
  },
  {
    name: "dungeon",
    path: "dungeon.mp3"
  },
  {
    name: "urgent",
    path: "urgent.mp3"
  },
  {
    name: "home",
    path: "home.mp3"
  }
];

// will be filled with the loaded audio objects where the key is name
var audioObjects = {};

function audioPreload () {
  var basePath = rootPath + '/audio/';
  audioAssets.forEach((audio) => {
    game.load.audio(audio.name, basePath + audio.path);
  });
}
/**
* @param callback is the audio load callback
*/
function audioLoad (callback) {
  var sounds = [];
  audioAssets.forEach((audio) => {
    var sound = game.add.audio(audio.name);
    sounds.push(sound);
    audioObjects[audio.name] = sound;
  });
  game.sound.setDecodedCallback(sounds, callback, this);
}
