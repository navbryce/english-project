var dialogue;
var gameComplete;
var tileEvents;
/* Must be called after conversationPreload */
function eventsPreload () {
  dialogue = {
    setting: {
      prologue: {
        name: 'prologue',
        lines: [
          'You and your wife were on your way',
          'to a new house when your car broke down.',
          'You find yourself in the cold, Austrian forest.'
        ]
      },
      intro: {
        name: 'intro',
        lines: [
          'A Mysterious Stranger',
          'Game by Bryce Plunkett'
        ]
      },
      observational: {
        name: 'mound',
        lines: [
          "It's some sort of mound. The earth hasn't been touched for years"
        ]
      },
      wolves_trap: {
        name: 'wolves',
        lines: [
          "You're trapped",
          ' between a rock',
          'and a pack of wolves.',
          'You MUST walk forward.'
        ]
      }
    },
    noise: {
      howling: {
        name: 'howling',
        lines: [
          '*Howling*',
          '*Howling*',
          'Better get to safety'
        ]
      }
    }
  };
  tileEvents = {
    sceneOne: {
      initial: { // objects so a lastnotified key can be placed on them
        text: dialogue.setting.intro.lines,
        title: true, // make true
        audio: {
          sound: 'forest',
          isMusic: true
        }
      },
      22: { // objects so a lastnotified key can be placed on them
        convo: 'first'
      },
      50: {
        text: dialogue.noise.howling.lines,
        title: true, // make true
        audio: {
          sound: 'wolf',
          isMusic: false
        }
      },
      90: {
        text: [
          "Those spikes look dangerous. Don't step on them or you'll lose points."
        ],
        title: false
      },
      137: {
        text: [
          "Be smart when talking to people. The longer it takes, the more you lose points."
        ],
        title: false
      },
      166: {
        text: dialogue.setting.observational.lines,
        title: false
      },
      213: {
        text: dialogue.setting.wolves_trap.lines,
        title: true, // make true
        show: [
          'wolf1',
          'wolf2'
        ]
      },
      227: {
        convo: 'wolves'
      },
      255: {
        text: [
          "The wolves vanished",
          "A mysterious man",
          "appears in front of you"
        ],
        title: true, // make true
        hide: [
          'wolf1',
          'wolf2'
        ],
        show: [
          'azzo-wolf'
        ],
      },
      256: {
        convo: 'azzo-first',
        end: {
          hide: [
            'azzo-wolf'
          ]
        }
      },
      272: {
        text: [
          "The sign reads: Ruins of Castle Klatka. Austrian Historical Society, 2001. You also note that some delinquents carved curse words onto the sign. Kids these days."
        ],
        title: false
      },
      326: {
        text: [
          "You look inside the door and see a crumbling statue. You also see an altar. This must have been the chapel."
        ],
        title: false
      },
      430: {
        text: [
          "This must have been the castle's main tower. There's glass on the ground. Be careful, Nike's can only help so much."
        ],
        title: false
      },
      490: {
        text: [
          "It's a weathered oak tree."
        ],
        title: false
      },
      629: {
        convo: 'mailbox'
      },
      704: {
        text: [
          "Home, sweet home.",
          "You hear someone go",
          "through the front door."
        ],
        title: true, // make true,
        audio: {
          sound: 'home',
          isMusic: true
        },
        level: 'conversationEvents',
        hide: [
          'initial-wife'
        ],
        show: [
          'wife-door'
        ]
      }
    },
    conversationEvents: {
      671: {
        convo: 'wife-return',
        level: 'dinner',
        end: {
          level: 'dinner',
          show: [
            'azzo-dinner',
            'wife-dinner'
          ],
          hide: [
            'wife-door'
          ],
          text: [
            "Later that night..."
          ],
          title: true, // make true
          end: {
            convo: 'dinner',
            end: {
              title: true, // make true
              hide: [
                'wife-dinner',
                'azzo-dinner'
              ],
              text: [
                'The next morning...'
              ],
              level: 'morning-after',
              movePlayer: {
                xTile: 708,
                yTile: 20
              },
              show: [
                'wife-stairs'
              ],
              end: {
                convo: 'morning-after',
                end: {
                  text: [
                    'One month later'
                  ],
                  title: true, // make true
                  end: {
                    level: 'climax',
                    text: [
                      "Your friend is on your walkway. Greet him."
                    ],
                    title: false,
                    hide: [
                      'wife-stairs'
                    ],
                    show: [
                      'woislaw-path'
                    ]
                  }
                }
              }
            }
          }
        }
      }
    },
    climax: {
      634: {
        convo: 'woislaw',
        end: {
          audio: {
            sound: 'urgent',
            isMusic: true
          },
          hide: [
            'woislaw-path'
          ],
          show: [
            'woislaw-tree'
          ]
        }
      },
      487: {
        convo: 'woislawTree'
      },
      435: {
        convo: 'secret',
        end: {
          level: 'end',
          audio: {
            sound: 'dungeon',
            isMusic: true
          },
          movePlayer: {
            xTile: 435,
            yTile: 72
          }
        }
      }
    },
    end: { /* the coffin events automatically added */
    }
  };
}
var minYEvents = {
  sceneOne: {
    555: {
      25: {
        text: [
          "The sign mentions something about a house in that direction--a crude drawing made after that. It also says there's a hole in the wall in front of you (walk through it)."
        ],
        title: false
      }
    }
  }
}
/**
* checks event
*
* @param level level
* @param x - x tile
**/
function event (level, x, y) {
  tileEvent(level, x);
  minEventY(level, x, y);
}

/**
* @param notification - notification object
*/
function notify (notification) {
  var notify = true; // send the notification by default
  var currentTime = new Date().getTime();
  var lastNotified;
  if ((lastNotified = notification.lastNotified) != null) {
    // make sure you're not sending the notification too many times
    notify = currentTime - lastNotified > 30 * 1000 && notification.repeat;
  }
  if (notify) {
    notification.lastNotified = currentTime;
    // found in world-notification.js
    addNotification(notification);
  }
}

function tileEvent (level, x) {
  var notification = null;
  var levelBlock;

  if ((levelBlock = tileEvents[level]) != null) {
    if (levelBlock[x] != null) {
      notification = levelBlock[x];
      notify(notification)
    }
  }
}
function minEventY (level, x, y) {
  // notifications that only occur below a certain y level
  var levelBlock;
  if ((levelBlock = minYEvents[level]) != null) {
    var xBlock;
    if ((xBlock = levelBlock[x]) != null) {
      var keys = Object.keys(xBlock);
      for (var yCounter = 0; yCounter < keys.length; yCounter++) {
        var yMin = keys[yCounter];
        if (y > yMin) {
          notify(xBlock[yMin]);
        } else {
          break; // leave the loop because the array is sorted from least to greatest
        }
      }
    }
  }
}
