var convo;

var currentConversation = null;

function conversationPreload () {
  convo = this.game.plugins.add(PhreakNation.Plugins.DialogManager);
}

function conversationListener (conversation, answerIndex, answerString) {
  var response = conversation.choose(answerIndex);
  var next = conversation.next();

  // check to see if the answer was an exit button
  if (next) {
    var text = fillInVariable(getConversationLines(conversation), conversation._config.variable);

    // conversation.answers() returns false when there are no answers but more conversation
    var answers;

    if (conversation.answers()) {
      answers = conversation.answers();
    } else if (conversation._config.next) {
      // the conversation has more dialog
      answers = ['Continue'];
    } else {
      // it's the last part of the conversation
      answers = null;
      currentConversation = null;

      // call the end notification here only if it doesn't autoclose
      if (conversation._config.autoClose == null) {
        // the conversation has ended call end notifcation
        (conversation.endNotification == null ||
        notify(conversation.endNotification));
      }
    }

    // check if it decision has score effect
    var score = conversation._config.score;
    if (!!score) {
      changeScore(score);
    }

    // is the conversation specified to autoclose
    if (!!conversation._config.autoClose) {
      diagToggle(false);
      currentConversation = null;
      // the conversation has ended call end notifcation
      (conversation.endNotification == null ||
      notify(conversation.endNotification));
    } else {
      diagSetText(text, answers, (exitIndex, exitString) => {
        conversationListener(conversation, exitIndex, exitString);
      });
    }

  } else if (!response) {
    /* should only occur if a button was clicked that wasn't an answer
     AKA exit/continue */
    currentConversation = null;
    diagToggle(false);
  }
}

function conversationLoad () {

}

function getConversationLines (conversation) {
  return conversation.actor().name() + ': ' + conversation.text();
}
/**
* Returns true if a conversation actually started
*/
function setCurrentConversation (conversation, convoId, endNotification) {
  var startedConversation;

  // make sure a conversation isn't currently occuring
  var startedConversation = currentConversation != null;
  var isAnnouncement = false;
  if (!startedConversation) {
    conversation.endNotification = endNotification; // store the end notification
    if (!!convoId) {
      startConversation(conversation, convoId);
    } else {
      startAnnouncement(conversation, endNotification);
      isAnnouncement = true;
    }
  }
  // tell the caller that the announcement/convo was processed
  return startedConversation || isAnnouncement;
}
function startConversation (conversation, id) {
  currentConversation = conversation; // update the current conversation
  conversation.play();

  // bind the listener
  conversationListener = conversationListener.bind(this);

  var text = getConversationLines(conversation);
  var answers = !!conversation.answers() ? conversation.answers() : ['Continue'];
  diagSetText(text, answers, (exitIndex, exitString) => {
    conversationListener(conversation, exitIndex, exitString);
  });
}

function startAnnouncement (announcement, endNotification) {
  /* automatically converts to a single line because the dialog box
  will convert the lines to the appropriate size

  the endNotification will be called without waiting
  */
  var text = '';
  announcement.forEach((line) => {
    text += line;
  });
  diagSetText(text, null, (index, exitString) => {
    diagToggle(false);
  });
  !!endNotification && notify(endNotification)
}
function fillInVariable (text, variable) {
  if (!!variable) {
    text = text.replace(/{{VARIABLE}}/g, variable);
  }
  return text;
}
