// debug flag
const debug = false;
const disableAudio = false;
const otherSideX = 677;

// general settings
const gameWidth = 1000;
const gameHeight = 600;
const rootPath = '/english-project'; // change to /english-project for gitlab
const initialX = 6; // starting x tile for player 6
const initialY = 20; // starting y tile for player

var level = 'sceneOne'; // starting level sceneOne

var game = new Phaser.Game(gameWidth, gameHeight, Phaser.CANVAS, 'game', { preload: preload, create: create, update: update, render: render });
function preload() {
  // add plugins
  game.plugins.add(PhreakNation.Plugins.DialogManager);

  // add resources
  var basePath = rootPath + "/tilemap";
  game.load.tilemap('back-layer', basePath + '/1_BackLayer.csv', null, Phaser.Tilemap.CSV);
  game.load.tilemap('back-layer-details', basePath + '/1_BackLayerDetails.csv', null, Phaser.Tilemap.CSV);
  game.load.tilemap('main-layer', basePath + '/1_MainLayer.csv', null, Phaser.Tilemap.CSV);
  game.load.tilemap('death-layer', basePath + '/1_DeathLayer.csv', null, Phaser.Tilemap.CSV);
  game.load.tilemap('front-layer', basePath + '/1_FrontLayer.csv', null, Phaser.Tilemap.CSV);

  game.load.image('tiles', basePath + '/tileset-one.png');
  game.load.image('tiles-two', basePath + '/tileset-two.png');

  game.load.spritesheet('player', basePath + '/sprites/spaceman.png', 16, 16);

  // background images
  this.game.load.image('front', basePath + '/background/front.png');
  this.game.load.image('mid', basePath + '/background/mid.png');
  this.game.load.image('back', basePath + '/background/back.png');

  audioPreload();
  conversationPreload();
  dialogPreload();
  eventsPreload();
  mapAssetsPreload();
  textPreload();
}
var backLayer;
var backDetailsLayer;
var frontMap, map, deathMap, backMap, backMapDetails;
var deathLayer;
var frontLayer;
var layer;
var cursors;
var player;
const resizeScale = 4;

function createAfterAudio () {
  addBackground();

  game.physics.startSystem(Phaser.Physics.ARCADE);

  //  Because we're loading CSV map data we have to specify the tile size here or we can't render it
  backMap = game.add.tilemap('back-layer', 16, 16);
  backMapDetails = game.add.tilemap('back-layer-details', 16, 16);
  map = game.add.tilemap('main-layer', 16, 16);
  deathMap = game.add.tilemap('death-layer', 16, 16);

  //  Now add in the tileset
  deathMap.addTilesetImage('tiles-two');
  deathMap.addTilesetImage('tiles');
  backMap.addTilesetImage('tiles-two');
  backMap.addTilesetImage('tiles');
  backMapDetails.addTilesetImage('tiles-two');
  backMapDetails.addTilesetImage('tiles');
  map.addTilesetImage('tiles-two');
  map.addTilesetImage('tiles');
  deathMap.addTilesetImage('tiles-two');
  deathMap.addTilesetImage('tiles');

  //  Create our layer
  backLayer = backMap.createLayer(0);
  backDetailsLayer = backMapDetails.createLayer(0);
  layer = map.createLayer(0);
  deathLayer = deathMap.createLayer(0);
  // load map assets
  mapAssetsLoad();
  // create player
  player = game.add.sprite(48, 48, 'player', 1);
  // create front map
  frontMap = game.add.tilemap('front-layer', 16, 16);
  frontMap.addTilesetImage('tiles-two');
  frontMap.addTilesetImage('tiles');
  frontLayer = frontMap.createLayer(0);

  //  Resize the world
  layer.setScale(resizeScale);
  layer.resizeWorld();
  backLayer.setScale(resizeScale);
  backLayer.resizeWorld();
  backDetailsLayer.setScale(resizeScale);
  backDetailsLayer.resizeWorld();
  deathLayer.setScale(resizeScale);
  deathLayer.resizeWorld();
  frontLayer.setScale(resizeScale);
  frontLayer.resizeWorld();

  //  This isn't totally accurate, but it'll do for now
  map.setCollisionBetween(1, 1000);
  deathMap.setCollisionBetween(1, 1000);


  //  Un-comment this on to see the collision tiles
  // layer.debug = true;

  //  Player (created earlier so behind the front layer)
  player.scale.setTo(resizeScale, resizeScale);
  player.animations.add('left', [8,9], 10, true);
  player.animations.add('right', [1,2], 10, true);
  player.animations.add('up', [11,12,13], 10, true);
  player.animations.add('down', [4,5,6], 10, true);
  game.physics.enable(player);
  game.physics.arcade.gravity.y = 900;

  player.body.linearDamping = 1;

  player.body.setSize(14, 14, 2, 1);

  // set player initial location
  player.x = convertToPx(initialX);
  player.y = convertToPx(initialY);

  // handle camera
  game.camera.follow(player);
  cursors = game.input.keyboard.createCursorKeys();

  text = game.add.text(gameWidth / 2 - 200, 40, '',
    {
      font: "20px Arial",
      fill: "white",
      stroke: "black",
      strokeThickness: 2
    });
  text.fixedToCamera = true;

  // conversation notifcations
  conversationLoad();

  // score
  scoreLoad();

  // in world-notification
  initialize();

  // determine end coffin
  determineCoffin();

  // check if the starting location has an event
  event(level, 'initial');
}

function create () {
  audioLoad(() => {

  });
  createAfterAudio();
}

function update () {
  const yV = 125;
  game.physics.arcade.collide(player, layer); // always have physics running
  game.physics.arcade.collide(player, mapAssetsGroup); // always have physics running

  // death collision
  game.physics.arcade.collide(player, deathLayer, function () {
    playerDeath();
  });


  player.body.velocity.x = 0;
  if (!titleScreen && (currentConversation == null || debug)) {
    if (cursors.left.isDown)
    {
        player.body.velocity.x = -100 * resizeScale;
        player.play('left');
        if (cursors.up.isDown && player.body.onFloor())
        {
            player.body.velocity.y = -yV * resizeScale;
            player.play('up');
        }
    }
    else if (cursors.right.isDown)
    {
        player.body.velocity.x = 100 * resizeScale;
        player.play('right');
        if (cursors.up.isDown && player.body.onFloor())
        {
            player.body.velocity.y = -yV * resizeScale;
            player.play('up');
        }
    }
    else if (cursors.up.isDown && player.body.onFloor())
    {
        player.body.velocity.y = -yV * resizeScale;
        player.play('up');
    }
    else
    {
        player.animations.stop();
    }

    // scroll backgrounds
    var cameraX = -game.camera.x;
    back.tilePosition.set(0 * cameraX, 0);
    mid.tilePosition.set(0.2 * cameraX, 0);
    front.tilePosition.set(.4 * cameraX, 0);
  } else {
    player.animations.stop();
  }
}

function render() {
  // game.debug.bodyInfo(player, 32, 32);
  // game.debug.body(player);

  // check for tile event
  event(level, layer.getTileX(player.x), layer.getTileX(player.y));
  renderScore(); // update score
  if (debug) {
    game.debug.text('Tile X: ' + layer.getTileX(player.x), 32, 48, 'rgb(0,0,0)');
    game.debug.text('Tile Y: ' + layer.getTileY(player.y), 32, 64, 'rgb(0,0,0)');
  }
}

var back, mid, front;
var background; // group layer
function addBackground () {
  background = game.add.group();

  const backgroundScale = 1.5;
  const startingLoc = 130 * (1/backgroundScale);

  // background
  back = game.add.tileSprite(0,
      startingLoc - game.cache.getImage('back').height,
      gameWidth,
      game.cache.getImage('back').height,
      'back'
  );
  back.scale.setTo(backgroundScale);
  back.fixedToCamera = true;
  mid = game.add.tileSprite(0,
      startingLoc - this.game.cache.getImage('mid').height,
      gameWidth,
      game.cache.getImage('mid').height,
      'mid'
  );
  mid.scale.setTo(backgroundScale);
  mid.fixedToCamera = true;
  front = game.add.tileSprite(0,
      startingLoc - game.cache.getImage('front').height,
      gameWidth,
      game.cache.getImage('front').height,
      'front'
  );
  front.scale.setTo(backgroundScale);
  front.fixedToCamera = true;

  background.add(back);
  background.add(mid);
  background.add(front);
}

function switchLevel (newLevel) {
  level = newLevel;
}

// randomly determine end coffin
var xTiles = [360, 333, 307];
function determineCoffin () {
  let index = ~~(Math.random() * xTiles.length);
  let actualCoffin = xTiles[index];
  xTiles.forEach((xTile) => {
    if (xTile === actualCoffin) {
      tileEvents.end[xTile] = {
        convo: 'correct-coffin',
        end: {
          gameComplete: true,
          audio: {
            sound: 'end',
            isMusic: true
          }
        }
      }
    } else {
      tileEvents.end[xTile] = {
        convo: 'incorrect-coffin'
      }
    }
  });
  let text;
  switch (index) {
    case 0:
      text = 'first';
      break;
    case 1:
      text = 'middle';
      break;
    default:
      text = 'last';
      break;
  }
  dialogConfig.woislawTree.dialog[2].variable = text;
}
