var score = 15000;
var scoreElement;
var scoreFont = 14;
var scoreFill = "yellow";
var scoreStyle = { font: "bold " + scoreFont * 1.5 + "px", fill: scoreFill };
var halt = false;
// time settings
var timeBetween = 100;
var timeEffect = 1;

function changeScore(pointIncrement) {
  if (!gameComplete) {
    score += pointIncrement;
  }
}

function scoreLoad() {
  scoreElement = game.add.text(150, 10, score, scoreStyle);
  scoreElement.fixedToCamera = true;
  game.time.events.add(timeBetween, timeDeduction, this);
}

function timeDeduction() {
  changeScore(-timeEffect);
  game.time.events.add(timeBetween, timeDeduction, this);
}

function renderScore() {
  scoreElement.text = 'Score: ' + score;
}
